# Gestion espace disque

cet outil permettant de surveiller l’utilisation de l’espace disque de votre machine. Il
pourra se lancer soit sans argument, en affichant un r ́esum ́e des informations pertinentes, ou bien avec des argu-
ments, afin d’obtenir des informations plus d ́etaill ́ees

## Usage

    project.sh [-OPTION] [REGEX] [DIRECTORY] [OUTPUT]

## Option

* -d [DIRECTORY] permettra de cibler un autre dossier que le répertoire courant

* -h affichera les tailles des différents dossiers de manière lisible pour un être humain (Ko, Mo, Go...)
* -s triera les résultats obtenus par ordre décroissant d’espace disque utilisé
* -r [REGEX] ciblera uniquement les éléments correspondant a l’expression reguliere passée en paramètre
* -f affichera  ́egalement les fichiers du dossier cibl ́e, en plus de ses sous-dossiers directs
* -a prendra en compte dans son affichage les fichiers et dossiers cachées
* -o [OUTPUT] renverra le résultat dans un fichier, en indiquant la date et l’heure du lancement du
script

Les options sont cumulables entre elles :

* project -hs
* project -h -s

## Contributor

* Ait Elmenceur Ilyès
