dr=./*
Command=
Sort="-n"
if [ $# -eq 0 ]; 
then
    echo 'Size Directory'| awk -F$' ' '{printf("%9s|%12s\n", $1, $2);}'
    du -d0 -h $(echo $dr) | awk -F$'\t' '{printf("%9s|%12s\n",$1 , $2);}'
else
    if echo $@ | grep -Eq '^.*-[A-Za-z]*a' ;
    then
        Command=$Command" -a"
    fi
    if echo $@ | grep -Eq '^.*-[A-Za-z]*f' ;
    then
        dr=$(find * -maxdepth 1)
    fi

    if echo $@ | grep -Eq '^.*-[A-Za-z]*h' ;
    then
        Command=$Command" -h"
    fi

    if echo $@ | grep -Eq '^.*-[A-Za-z]*d' ;
    then
        if echo $@ | grep -Eq '^.*-[A-Za-z]*o' ;
        then
            dir=${@: -2:1}
        else
            dir=${@: -1}
        fi
        if [ -d $dir  ] ;
        then
            if echo $@ | grep -Eq '^.*-[A-Za-z]*f' ;
            then
                dr=$(find $dir/*)
            else
                dr=$dir
            fi
        else
            echo $Directory doesn\'t exist
            exit -1
        fi
    fi
    if echo $@ | grep -Eq '^.*-[A-Za-z]*s' ;
    then
        Sort="-rn"
    else
        Sort="-n"
    fi

    if echo $@ | grep -Eq '^.*-[A-Za-z]*r' ;
    then
        if echo $@ | grep -Eq '^.*-[A-Za-z]*o' ;
        then
            if echo $@ | grep -Eq '^.*-[A-Za-z]*d' ;
            then
                greppos=${@: -3:1}
            else
                greppos=${@: -2:1}
            fi
        else
            greppos=${@: -1}
        fi
    else
        greppos='^.*$'
    fi

    if echo $@ | grep -Eq '^.*-[A-Za-z]*o' ; 
        then
            echo 'Size Directory'| awk -F$' ' '{printf("%9s|%12s\n", $1, $2);}'> ${@: -1}
            du -d1 $Command $(echo $dr) | awk -F$'\t' '{printf("%9s|%12s\n", $1, $2);}'| sort $Sort -t $'|' -k1,1 |grep -E "$greppos">> ${@: -1}
        else
            echo 'Size Directory'| awk -F$' ' '{printf("%9s|%12s\n", $1, $2);}'
            du -d1 $Command $(echo $dr) | awk -F$'\t' '{printf("%9s|%12s\n", $1, $2);}'| sort $Sort -t $'|' -k1,1|grep -E "$greppos"
    fi
fi